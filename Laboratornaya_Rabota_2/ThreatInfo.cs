﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratornaya_Rabota_2
{
    [Serializable]
    struct ThreatInfo
    {
        public int Id { set; get; } //ID угрозы
        public string Name { set; get; } //Наименование угрозы
        public string Description { set; get; } //Описание угрозы
        public string Sourse { set; get; } //Источник угрозы
        public string ObjImpact { set; get; } //Объект воздействия угрозы
        public bool Privacy { set; get; } //Нарушение конфиденциальности 
        public bool Integrity { set; get; } //Нарушение целостности
        public bool Access { set; get; } //Нарушение доступности

        public string IdString { set; get; } //ID угрозы в формате УБИ.ID
    }
}
