﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using System.Net;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Runtime.Serialization.Formatters.Binary;

namespace Laboratornaya_Rabota_2
{
    public partial class MainWindow : Window
    {
        string PathXLSX { set; get; } = "thrlist.xlsx";
        string PathBIN { set; get; } = "data.bin";

        Dictionary<int, ThreatInfo> dictThreat = new Dictionary<int, ThreatInfo>();
        public MainWindow()
        {
            if (!File.Exists(PathBIN))
            {
                if (!File.Exists(PathXLSX))
                {
                    MessageBox.Show("Данные не обнаружены на вашем компьтере, необходима загрузка из сети Интернет");
                    string webAddress = "https://bdu.fstec.ru/files/documents/thrlist.xlsx";
                    WebClient web = new WebClient();
                    web.DownloadFile(webAddress, PathXLSX);
                    MessageBox.Show("Данные успешно загружены на ваш компьютер, чтобы обновить, нажмите кнопку \"Обновить данные\"");
                }
                MessageBox.Show("Данные уже загружены, чтобы обновить, нажмите кнопку \"Обновить данные\"");
                ParsingData();
            }
            else
            {
                MessageBox.Show("Данные уже загружены, чтобы обновить, нажмите кнопку \"Обновить данные\"");
                DeserializeData();
            }
            InitializeComponent();
            
            List<ThreatInfo> dataGridList = new List<ThreatInfo>();
            for (int i = 1; i <= 15; i++)
            {
                dataGridList.Add(dictThreat[i]);
            }
            DataThreats.ItemsSource = dataGridList;
            MoreInfoBlock.Text = "Выберите угрозу из списка, чтобы узнать о ней подробнее";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            string webAddress = "https://bdu.fstec.ru/files/documents/thrlist.xlsx";
            WebClient web = new WebClient();
            try
            {
                web.DownloadFile(webAddress, PathXLSX);
            }
            catch (WebException exc)
            {
                MessageBox.Show("Нет подключения к Интернету\n" + exc.Message);
                return;
            }
            MessageBox.Show("Данные успешно обновленны");
            ParsingData();

            this.Cursor = Cursors.Arrow;
        }

        private void ParsingData()
        {
            var newDict = new Dictionary<int, ThreatInfo>();
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(PathXLSX, false))
            {
                int i = 3;
                while (GetCellValue(document, "A" + i.ToString()) != null)
                {
                    int id = int.Parse(GetCellValue(document, "A" + i.ToString()));
                    if (!newDict.ContainsKey(id))
                    {
                        ThreatInfo info = new ThreatInfo();
                        info.Id = id;
                        info.IdString = "УБИ." + id.ToString();
                        info.Name = GetCellValue(document, "B" + i.ToString());
                        info.Description = GetCellValue(document, "C" + i.ToString());
                        info.Sourse = GetCellValue(document, "D" + i.ToString());
                        info.ObjImpact = GetCellValue(document, "E" + i.ToString());
                        
                        if (GetCellValue(document, "F" + i.ToString()) == "1")
                            info.Privacy = true;
                        else
                            info.Privacy = false;

                        if (GetCellValue(document, "G" + i.ToString()) == "1")
                            info.Integrity = true;
                        else
                            info.Integrity = false;

                        if (GetCellValue(document, "H" + i.ToString()) == "1")
                            info.Access = true;
                        else
                            info.Access = false;
                        newDict.Add(id, info);
                    }
                    i++;
                }
            }

            AnalysisData(dictThreat, newDict);
            dictThreat = newDict;
            File.Delete(PathBIN);

            using (FileStream file = File.Open(PathBIN,FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, dictThreat);
            }

            List<ThreatInfo> dataGridList = new List<ThreatInfo>();
            for (int i = 1; i <= 15; i++)
            {
                dataGridList.Add(dictThreat[i]);
            }
            DataThreats.ItemsSource = dataGridList;
            MessageBox.Show("Успешно сохранено");
        }



        private string GetCellValue(SpreadsheetDocument document, string address)
        {
            string value = null;

            WorkbookPart wbPart = document.WorkbookPart;
            Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().First();

            WorksheetPart wsPart = (WorksheetPart)wbPart.GetPartById(theSheet.Id);

            Cell theCell = wsPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == address).FirstOrDefault();

            if (theCell != null)
            {
                value = theCell.InnerText;
                if (theCell.DataType != null)
                {
                    var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                    value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                }
            }

            return value;
        }

        private void AnalysisData(Dictionary<int, ThreatInfo> oldDict, Dictionary<int, ThreatInfo> newDict)
        {
            List<string> oldRecord = new List<string>();
            List<string> editRecord = new List<string>();

            foreach (var item in oldDict)
            {
                if (!newDict.ContainsKey(item.Key))
                {
                    oldRecord.Add(item.Value.IdString);
                }
                else
                {
                    ThreatInfo newInfo = newDict[item.Key];
                    if (item.Value.Name != newInfo.Name)
                        editRecord.Add($"{item.Value.IdString} Наименование БЫЛО: {item.Value.Name} СТАЛО: {newInfo.Name}");
                    if (item.Value.Description != newInfo.Description)
                        editRecord.Add($"{item.Value.IdString} Описание БЫЛО: {item.Value.Description} СТАЛО: {newInfo.Description}");
                    if (item.Value.Sourse != newInfo.Sourse)
                        editRecord.Add($"{item.Value.IdString} Источник угрозы БЫЛО: {item.Value.Sourse} СТАЛО: {newInfo.Sourse}");
                    if (item.Value.ObjImpact != newInfo.ObjImpact)
                        editRecord.Add($"{item.Value.IdString} Объект воздействия БЫЛО: {item.Value.ObjImpact} СТАЛО: {newInfo.ObjImpact}");
                    if (item.Value.Privacy != newInfo.Privacy)
                        editRecord.Add($"{item.Value.IdString} Нарушение конфиденциальности БЫЛО: {item.Value.Privacy} СТАЛО: {newInfo.Privacy}");
                    if (item.Value.Integrity != newInfo.Integrity)
                        editRecord.Add($"{item.Value.IdString} Нарушение целостности БЫЛО: {item.Value.Integrity} СТАЛО: {newInfo.Integrity}");
                    if (item.Value.Access != newInfo.Access)
                        editRecord.Add($"{item.Value.IdString} Нарушение доступности БЫЛО: {item.Value.Access} СТАЛО: {newInfo.Access}");
                }
            }
            List<string> newRecord = new List<string>();
            foreach (var item in newDict)
            {
                if (!oldDict.ContainsKey(item.Key))
                {
                    newRecord.Add(item.Value.IdString);
                }
            }

            if (oldRecord.Count == 0 && newRecord.Count == 0 && editRecord.Count == 0)
            {
                MessageBox.Show("Никаких изменений, можно было и не обновлять");
                return;
            }
            else
            {
                string output = "";
                if (newRecord.Count != 0)
                {
                    output = "ID новых записей:";
                    foreach (var item in newRecord)
                    {
                        output += "\n" + item + "\n";
                    }
                }
                if (oldRecord.Count != 0)
                {
                    output += "ID удаленных записей:";
                    foreach (var item in oldRecord)
                    {
                        output += "\n" + item + "\n";
                    }
                }
                if (editRecord.Count != 0)
                {
                    output += "Изменения в записях:";
                    foreach (var item in editRecord)
                    {
                        output += "\n" + item + "\n";
                    }
                }
                MessageBox.Show(output);
                return;
            }
        }

        private void DeserializeData()
        {
            using (FileStream file = File.Open(PathBIN, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                dictThreat = (Dictionary<int, ThreatInfo>)formatter.Deserialize(file);
            }
            
        }

        private void Button_Click_MoveBack(object sender, RoutedEventArgs e)
        {
            var list = (List<ThreatInfo>)DataThreats.ItemsSource;
            int firstId = list[0].Id - 15;
            int lastId = list[0].Id - 1;

            if (firstId < 0)
                return;

            List<ThreatInfo> dataGridList = new List<ThreatInfo>();
            for (int i = firstId; i <= lastId; i++)
            {
                if (dictThreat.ContainsKey(i))
                    dataGridList.Add(dictThreat[i]);
            }
            DataThreats.ItemsSource = dataGridList;
        }

        private void Button_Click_MoveForward(object sender, RoutedEventArgs e)
        {
            var list = (List<ThreatInfo>)DataThreats.ItemsSource;
            int firstId = list[0].Id + 15;
            int lastId = firstId + 14;

            if (firstId > dictThreat.Count)
                return;

            List<ThreatInfo> dataGridList = new List<ThreatInfo>();
            for (int i = firstId; i <= lastId; i++)
            {
                if (dictThreat.ContainsKey(i))
                    dataGridList.Add(dictThreat[i]);
            }
            DataThreats.ItemsSource = dataGridList;
        }

        private void DataThreats_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedCell = (ThreatInfo)DataThreats.SelectedCells.First().Item;
            string output = $"ID угрозы: {selectedCell.IdString}\n" +
                $"\nНаименование угрозы: {selectedCell.Name}\n" +
                $"\nОписание угрозы: {selectedCell.Description}\n" +
                $"\nИсточник угрозы: {selectedCell.Sourse}\n" +
                $"\nОбъект воздействия угрозы: {selectedCell.ObjImpact}\n";

            if (selectedCell.Privacy)
                output += "\nНарушение конфиденциальности: ДА\n";
            else
                output += "\nНарушение конфиденциальности: НЕТ\n";

            if (selectedCell.Integrity)
                output += "Нарушение целостности: ДА\n";
            else
                output += "Нарушение целостности: НЕТ\n";

            if (selectedCell.Access)
                output += "Нарушение доступности: ДА\n";
            else
                output += "Нарушение доступности: НЕТ\n";

            MoreInfoBlock.Text = output;
        }
    }
}

